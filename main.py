import yaml
import subprocess
from multiprocessing import Pool, cpu_count
from tqdm import tqdm

import glob
import os
import argparse

# add any dependencies you need here, that might have been used 
# to create the objects in the pickle files and the h5 files

parser = argparse.ArgumentParser(description='Compresses all the pickle and h5 files in the current directory')
parser.add_argument('--decompress', action='store_true', help='Decompresses all the lzma files in the current directory')
parser.add_argument('--compress', action='store_true', help='Compresses all the pickle and h5 files in the current directory')
parser.add_argument('--method', type=str, default='lzma', help='Compression method to use')
parser.add_argument('--skip_if_exists', action='store_true', help='Whether to skip compression/decompression if the file already exists')
parser.add_argument('--report_stats', action='store_true', help='Whether to report the compression/decompression ratio of each file')
parser.add_argument('--generate_list', action='store_true', help='Whether to generate a list of all the models and their filters')
parser.add_argument('--update_list', action='store_true', help='Whether to update the list of all the models and their filters, not generating a new one')

def compress_file(arguments):
    file_path, method, skip_if_exists, output_dir = arguments
    if method is None:
        method = 'lzma'
    if skip_if_exists is None:
        skip_if_exists = True
    if output_dir is None:
        output_dir = 'models'
    if method == 'lzma':
        if skip_if_exists and os.path.exists(output_dir + '/' + file_path.split('/')[-1] + '.lzma'):
            return
        # with subprocess, compress the file
        stdout, stderr = subprocess.Popen(['lzma', '-z', '-k', file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        if stderr.decode('utf-8') != '' and 'File exists' not in stderr.decode('utf-8'):
            raise RuntimeError(f'Error compressing {file_path}: {stderr}')
        # first check if the output directory exists
        os.makedirs(output_dir, exist_ok=True)
        os.rename(file_path + '.lzma', output_dir + '/' + file_path.split('/')[-1] + '.lzma')
    else:
        raise NotImplementedError
    
def decompress_file(arguments):
    compressed_file_path, skip_if_exists, output_dir = arguments
    if skip_if_exists is None:
        skip_if_exists = True
    if output_dir is None:
        output_dir = 'decompressed_models'
    
    if skip_if_exists and os.path.exists(output_dir + '/' + compressed_file_path.split('/')[-1].replace('.lzma', '')):
        return
    
    os.makedirs(output_dir, exist_ok=True)
    stdout, stderr = subprocess.Popen(['lzma', '-d', '-k', compressed_file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    if stderr.decode('utf-8') != '' and 'File exists' not in stderr.decode('utf-8'):
        raise RuntimeError(f'Error decompressing {compressed_file_path}: {stderr}')
    os.rename(compressed_file_path.replace('.lzma', ''), output_dir + '/' + compressed_file_path.split('/')[-1].replace('.lzma', ''))

if __name__ == '__main__':
    args = parser.parse_args()
    if args.compress and args.decompress:
        raise ValueError('Cannot compress and decompress at the same time')
    elif args.compress:
        files = []
        # grab all the directories in uncompressed_models
        dirs = glob.glob('uncompressed_models/**/', recursive=True)
        for dir in dirs:
            # grab all the files in each directory
            files += glob.glob(dir + '**', recursive=True)

        # remove the dirs themselves from the list of files
        files = [file for file in files if not os.path.isdir(file)]
        # remove duplicates
        files = list(set(files))

        # compress all the files
        files_to_compress = []

        for i, file in enumerate(files):
            # if the file is not just in uncompressed_models but a directory within it
            # use that directory as the output directory too
            outdir = 'models'
            if os.path.dirname(file) != 'uncompressed_models':
                outdir = os.path.join('models', str(os.path.dirname(file)).split('/')[-1])
            files_to_compress.append((file, args.method, args.skip_if_exists, outdir))
        
        # use all the cores
        with Pool(cpu_count()) as p:
            list(tqdm(p.imap(compress_file, files_to_compress), total=len(files_to_compress)))

        if args.report_stats:
            # compare the sizes before and after compression
            for file in files:
                original_size = os.path.getsize(file)
                compressed_size = os.path.getsize(file.replace("uncompressed_models", "models") + '.lzma')

                ratio = original_size / compressed_size

                # show the size in MB
                original_size /= 1024**2
                compressed_size /= 1024**2

                print(f'{file} compression ratio: {round(ratio, 2)}: {round(original_size, 2)} MB -> {round(compressed_size, 2)} MB')

        if args.generate_list or args.update_list:
            # create a dictionary with the model names as the keys, and the filters as the values
            # grab the directories in models
            dirs = glob.glob('models/**/')
            # only keep the directories, not the "models/" part
            dirs = [dir.split('/')[-2] for dir in dirs]

            if args.update_list:
                try:
                    with open('models.yaml', 'r') as f:
                        model_dict = yaml.load(f, Loader=yaml.FullLoader)
                except FileNotFoundError:
                    model_dict = {}

            for dir in dirs:
                if args.update_list and dir in model_dict:
                    continue

                model_dict[dir] = {}

            # for each directory, grab all the files in it
            for dir in dirs:
                files = glob.glob('models/' + dir + '/*')
                model_dict[dir]["filters"] = sorted(set([file.split("/")[-1].split(".")[0] for file in files]) | set(model_dict[dir].get("filters", [])))

            # sort the keys alphabetically
            model_dict = {k: model_dict[k] for k in sorted(model_dict)}

            with open('models.yaml', 'w') as f:
                yaml.dump(model_dict, f)
            
    elif args.decompress:
        # grab all the lzma files in the current directory
        files = glob.glob('**/*.lzma', recursive=True)
        # remove duplicates
        files = list(set(files))
        files_to_decompress = []
        for i, file in enumerate(files):
            outdir = 'decompressed_models'
            if os.path.dirname(file) != '':
                outdir = file.replace('models', 'decompressed_models').replace(file.split('/')[-1], '')
            files_to_decompress.append((file, args.skip_if_exists, outdir))

        # use all the cores
        with Pool(cpu_count()) as p:
            list(tqdm(p.imap(decompress_file, files_to_decompress), total=len(files_to_decompress)))
