# NMMA models

This is a gitlab repo used to host the NMMA models. A script is also provided to prepare the models and to generate the `models.yaml` file used by the NMMA python package when fetching models. The `benchmarks` folder contains plots quantifying the training performance of each model.
